#
#  Be sure to run `pod spec lint RWPickFlavor.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  #1
  spec.platform = :ios
  spec.ios.deployment_target = '12.0'
  spec.name         = "RWPickFlavor"
  spec.summary      = "RWPickFlavor lets a user select an ice cream flavor."
  spec.requires_arc = true

  #2
  spec.version = "0.1.0"

  #3
  spec.license      = { :type => "MIT", :file => "LICENSE" }

  #4
  spec.author             = { "Chamara Thennakoon" => "chamara.p@koku.io" }

  #5
  spec.homepage     = "https://bitbucket.org/chamarathennakoon/rwpickflavor"

  #6
  spec.source       = { :git => "https://bitbucket.org/chamarathennakoon/rwpickflavor.git", :tag => "#{spec.version}" }

  # 7
  # spec.framework = "UIKit"
  # spec.dependency 'Alamofire', '~> 4.7'
  # spec.dependency 'MBProgressHUD', '~> 1.1.0'
  spec.framework = "UIKit"
  spec.framework = "AVFoundation"
  spec.framework = "CoreMotion"
  spec.framework = "CoreTelephony"
  spec.framework = "SystemConfiguration"
  # spec.framework = "AAILivenessSDK"
  # spec.dependency 'Alamofire', '~> 4.9.0'
  # spec.dependency 'SwiftyJSON', '~> 4.2.0'
  # spec.dependency 'DropDown'
  # spec.dependency 'IQKeyboardManagerSwift'
  # spec.dependency 'SVProgressHUD'
  # spec.dependency 'JVFloatLabeledTextField'
  spec.vendored_frameworks = 'ekyc.framework'

  # 8
  # spec.source_files = "RWPickFlavor/**/*.{swift}"

  # 9
  # spec.resources = "RWPickFlavor/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

  # 10
  spec.swift_version = "4.2"

end
